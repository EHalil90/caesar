$(document).ready(function(){
    $("#select-lang").change(function(){ langs.s = $("#select-lang").val(); });
    $("#input-shift").on("keypress", function(obj){ ValidateInput(obj); });
    $("#btn-encrypt").click(function(obj){ EncryptInputClick(obj); });
    
    langs.s = $("#select-lang").val();
});

let langs = {"s" : "",
             "sh": 0,
             "ru": ["русский", 33, 1040, 1071],
             "en": ["английский", 26, 65, 90],};

function ValidateInput(obj){
    if (obj.keyCode < 48 || obj.keyCode > 57){
        obj.preventDefault();
        obj.returnValue = false;
    }
}
function EncryptInputClick(obj){
    let sh = Number($("#input-shift").val());
    
    if (sh > langs[langs.s][1] - 1){
        alert("Выбран " + langs[langs.s][0] + " язык. Значение сдвига не может привышать " + (langs[langs.s][1] - 1));
        langs.sh = 0;
        return;
    }
    
    langs.sh = sh;
    
    let txtIn = $("#input-textIn").val();
    let txtOut = txtIn.split('').map(val => ShiftTheChar(val.toUpperCase())).join('');
    
    $("#input-textOut").val(txtOut);
    
    console.log(langs);
}

function ShiftTheChar(ch){
    if (ch.charCodeAt() == 32) return " ";
    if (ch.charCodeAt() < langs[langs.s][2] || ch.charCodeAt() > langs[langs.s][3]) return "_";
    
    return String.fromCharCode(ch.charCodeAt() + langs.sh).toUpperCase();
}
